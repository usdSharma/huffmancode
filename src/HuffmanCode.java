/**
 * University of San Diego
 * COMP 285
 * Spring 2015
 * Instructor: Gautam Wilkins
 *
 * This class implements Huffman codes for files containing ASCII characters. It can read and compress a file and also
 * decode a compressed file and covert it back to its original ASCII characters.
 */

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;



public class HuffmanCode {

    /**
     * Opens a file containing ascii characters and returns them as an ArrayList of characters (e.g.
     * if the file contents were: "abc\nd ef" then the resulting ArrayList would be:
     * {'a', 'b', 'c', '\n', 'd', ' ', 'e', 'f'}
     */
    public static ArrayList<Character> charactersFromFile(String fileName) {
        ArrayList<Character> fileChars = new ArrayList<Character>();
        File f = new File(fileName);
        try {
            FileReader fr = new FileReader(f);
            int next;
            while ((next = fr.read()) != -1) {
                Character current = (char) next;
                fileChars.add(current);
            }

        } catch (IOException e) {
            System.out.println("Could not open file: " + fileName);
            System.exit(1);
        }
        return fileChars;
    }

    /**
     * Takes an array of characters and returns a HashMap where each key is a character in the array
     * the value is how many times that character appears in the array. (e.g. for {a, b, c, a, a, d, c}
     * the HashMap would be:
     * {a->3, b->1, c->2, d->1}
     */
    public static HashMap<Character, Integer> frequenciesFromArray(ArrayList<Character> charArray) {
        HashMap<Character, Integer> ret = new HashMap<Character, Integer>();
        for(Character c : charArray) {
            if(!ret.containsKey(c)) {
                    int count = 0;
                for(Character ch : charArray) {
                    if(ch == c) {//proper char equality check?
                        count++;
                    }
                }
                ret.put(c,count);
            }
        }
        return ret;
    }

    /**
     * Takes a HashMap with character frequencies and generates a HuffmanTree to optimally encode
     * the individual characters.
     */
    public static HuffmanTree generateCodingTree(HashMap<Character, Integer> charFreq) {
        // Implement Me!
        PriorityQueue<HuffmanTree> q = new PriorityQueue<HuffmanTree>();
        for(Character c : charFreq.keySet()) {
            q.add(new HuffmanTree(c,charFreq.get(c)));//how do I make this sort in ascending value? automatic?
        }
        if(q.isEmpty()) {
            System.out.println("No message given. returning null coding tree");
            return null;
        }
        //check if this is correct: dequeue two items w/ lowest freqs, merge, and place back in queue
        HuffmanTree left = null;
        while(!q.isEmpty()) {
            left = q.remove();
            if(!q.isEmpty()) {
                HuffmanTree right = q.remove();
                q.add(HuffmanTree.merge(left,right));
            }
        }
        return left;
        // Hints:
        //
        // 1) Use the Java PriorityQueue class
        //
        // 2) To create a single Huffman Tree for a character (call it value) and a frequency, call:
        //      new HuffmanTree(char, frequency)
        //    You would use this at the setup stage when you create a Huffman tree with a single node
        //    for every character
        //
        // 3) To merge two Huffman trees (named left and right) call
        //    HuffmanTree parent = HuffmanTree.merge(left, right);
        //
        //    parent will have its frequency set to the sum of the frequencies of left and right, so
        //    you can immediately add it to the priority queue.
    }


    /**
     * Takes an ArrayList of characters and a HashMap of character encodings and then
     * generates the String encoding for charArray (e.g. if
     * charArray = {a, b, c, c, a, a}
     * and codeMap = {a->{true}, b->{false, true, false}, c->{false, false}}
     * then the resulting string will be:
     * 1010000011, where
     *
     * 1 010 00 00 1 1
     * a  b  c  c  a a
     */
    public static String encodeCharacters(ArrayList<Character> charArray, HashMap<Character, ArrayList<Boolean>> codeMap) {
        String bitString = "";
        for (Character c : charArray) {
            String cstring = BinaryHelper.bitStringFromBooleanList(codeMap.get(c));
            bitString = bitString.concat(cstring);
        }
        return bitString;
    }


    /**
     * Helper method to print character encodings in a quickly readable format
     */
    public static void printEncoding(HashMap<Character, ArrayList<Boolean>> encoding) {
        for (Map.Entry<Character, ArrayList<Boolean>> entry : encoding.entrySet()) {
            System.out.println(entry.getKey() + ": " + BinaryHelper.bitStringFromBooleanList(entry.getValue()));
        }
    }












}
